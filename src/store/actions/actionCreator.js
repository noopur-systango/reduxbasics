import { SUBTRACT,STORE_RESULT } from './actionConst';

//implementing action creators
export const subtract = (value) => {
    return {
        type: SUBTRACT,
        val: value
    }
}

export const saveResult = (res) => {
    return {
        type:STORE_RESULT,
        result:res
    }
}

//achieving asynchronous code with redux-thunk
export const storeResult = (res) =>
{
    return (dispatch, getState) => {
        setTimeout(() => {
            const oldCOunter = getState().ctr.counter;
            console.log(oldCOunter);
            dispatch(saveResult(res))        //middleware
        },2000) 
    }
}
