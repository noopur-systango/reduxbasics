import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import counterReducer from "./store/reducer/counter";
import resultReducer from "./store/reducer/results";
import { createStore, combineReducers, applyMiddleware, compose} from "redux";
import { Provider } from "react-redux";
import thunk from 'redux-thunk';

const rootReducer = combineReducers({       //multiple reducer in rootreducer
  ctr: counterReducer,
  res: resultReducer
});

const logger = store => {                   //creating middleware
  return next => {
    return action => {
      console.log("Middleware Dispatching", action);
      const result = next(action);
      console.log("Middleware next state", store.getState());
      return result;
    };
  };
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;  //for redux devtools
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(logger, thunk )));

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();
